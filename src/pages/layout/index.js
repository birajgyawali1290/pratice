import LoginPageLayout from "./loginpage.layout"
import RegisterPageLayout from "./registerpage.layout"
const Layout = {
    LoginPageLayout,
    RegisterPageLayout
}
export default Layout