import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { auth_svc } from "../../service/auth.service";
import { toast } from "react-toastify";

const LoginPageLayout = () => {
  let [data, setData] = useState({
    email: null,
    password: null,
  });

  const onChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async(e) => {
    try {
      e.preventDefault();
      let user = await auth_svc.login(data);
      toast.success("Welcome to "+ user.role+ " panel")
    } catch (error) {
      if(error?.response?.status === 400){
        if(error.response.data.msg){
            toast.error(error.response.data.msg)
        }
      }
    }
  };
  return (
    <>
      <div className="login-page">
        <Container>
          <Row className="justify-content-center align-items-center">
            <Col md={6}>
              <div className="login-form">
                <h1 className="text-center ">Login</h1>
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      name="email"
                      type="email"
                      placeholder="Enter email"
                      //   value={}
                      onChange={onChange}
                    />
                  </Form.Group>

                  <Form.Group controlId="formBasicPassword" className="my-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      name="password"
                      type="password"
                      placeholder="Password"
                      //   value={password}
                      onChange={onChange}
                    />
                  </Form.Group>

                  <Button variant="danger" type="reset" className="me-2">
                    Clear
                  </Button>
                  <Button variant="success" type="submit">
                    Login
                  </Button>
                </Form>
                <NavLink to={"/register"}>Register</NavLink>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default LoginPageLayout;
