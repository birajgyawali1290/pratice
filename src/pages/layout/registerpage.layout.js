import React, { useState } from "react";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useFormik } from "formik";
import * as Yup from "yup";
import { auth_svc } from "../../service/auth.service";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const RegisterPageLayout = () => {
    let navigator = useNavigate()
  let defaultValues = {
    name: "",
    email: "",
    password: "",
    role: "",
    status: "",
    address: null,
    mobile: "",
    image: "",
  };
  const validate = Yup.object({
    name: Yup.string().required("Please enter your full name").min(3),
    email: Yup.string().email().required("Email is required"),
    password: Yup.string().required("Password is required! "),
    role: Yup.string().required("Role is required"),
    status: Yup.string().required(),
    address: Yup.string().nullable(),
    mobile: Yup.number(),
  });
  const formik = useFormik({
    initialValues: defaultValues,
    validationSchema: validate,
    onSubmit: (values) => {
        console.log("values", values)
    },
  });

  const imageFilter = (e) => {
    let { files } = e.target;
    let file = files[0];
    let ext = file.name.split(".");
    ext = ext.pop();
    let allowed_image = ["jpg", "jpeg", "png", "webp", "svg", "gif", "bmp"];
    if (allowed_image.includes(ext.toLowerCase())) {
      if (file.size <= 5 * 1000 * 1000) {
        formik.setValues({
          ...formik.values,
          image: file,
        });
      } else {
        formik.setErrors({
          ...formik.errors,
          image: "File size should be less than 10 mb.",
        });
      }
    } else {
      formik.setErrors({
        ...formik.errors,
        image: "File format invalid",
      });
    }
  };
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      formik.handleSubmit();
      let values = {
        name: formik.values.name,
        email: formik.values.email,
        password: formik.values.password,
        role: formik.values.role,
        status: formik.values.status,
        mobile: formik.values.mobile,
        address: formik.values.address,
        image: formik.values.image.name,
      };
      let response = await auth_svc.register(values);
      if(response.status){
        navigator('/')
      }
    } catch (error) {
      console.log("Register Error", error);
    }
  };
  return (
    <>
      <div className="login-page">
        <Container>
          <Row className="justify-content-center align-items-center">
            <Col md={6}>
              <div className="login-form">
                <h2>Register</h2>
                <Form onSubmit={handleSubmit}>
                  <Form.Group controlId="formBasicName">
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      name="name"
                      type="text"
                      placeholder="Enter your name"
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors.name}</span>
                  </Form.Group>

                  <Form.Group controlId="formBasicEmail" className="my-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      name="email"
                      type="email"
                      placeholder="Enter email"
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors.email}</span>
                  </Form.Group>

                  <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      name="password"
                      type="password"
                      placeholder="Password"
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">
                      {formik.errors.password}
                    </span>
                  </Form.Group>

                  <Form.Group controlId="formBasicRole" className="my-3">
                    <Form.Label>Role</Form.Label>
                    <Form.Select
                      name="role"
                      as="select"
                      onChange={formik.handleChange}
                    >
                      <option value={"" ? formik.errors?.role : ""}>
                        --Select option--
                      </option>
                      <option value={"customer"}>Customer</option>
                      <option value={"seller"}>Seller</option>
                    </Form.Select>
                    <span className="text-danger">{formik.errors.role}</span>
                  </Form.Group>

                  <Form.Group controlId="formBasicRole">
                    <Form.Label>Status</Form.Label>
                    <Form.Select
                      name="status"
                      as="select"
                      onChange={formik.handleChange}
                    >
                      <option value={"" ? formik.errors?.status : ""}>
                        --Select option--
                      </option>
                      <option value={"active"}>Active</option>
                      <option value={"inactive"}>Inactive</option>
                    </Form.Select>
                    <span className="text-danger">{formik.errors.status}</span>
                  </Form.Group>

                  <Form.Group controlId="formBasicMobile" className="my-3">
                    <Form.Label>Mobile</Form.Label>
                    <Form.Control
                      name="mobile"
                      type="text"
                      placeholder="Enter your mobile number"
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors.mobile}</span>
                  </Form.Group>

                  <Form.Group controlId="formBasicAddress" className="mb-3">
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                      name="address"
                      type="text"
                      placeholder="Enter your address"
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors.address}</span>
                  </Form.Group>

                  <Form.Group controlId="formBasicImage" className="mt-3 mb-5">
                    <Form.Label>Image</Form.Label>
                    <Form.Control
                      type="file"
                      accept="image/*"
                      onChange={imageFilter}
                    />
                    {formik.values?.image && (
                      <img
                        src={URL.createObjectURL(formik.values.image)}
                        className="img img-fluid w-25 mt-2"
                      />
                    )}
                    <span className="text-danger">{formik.errors.image}</span>
                  </Form.Group>

                  <Button variant="success" type="submit">
                    Register
                  </Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default RegisterPageLayout;
