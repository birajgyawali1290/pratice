import axiosInstance from "../config/axios_connect";

class HttpService{
    header={};

    getHeaders = (config) => {
        if(config.login){
            let token = localStorage.getItem("auth_token");
            this.header = {
                "authorization": "Bearer "+token,
                "content-type": "application/json"
            }
        }
        if(config.files){
            this.header = {
                ...this.header,
                'content-type':'multipart/form-data'
            }
        }
    }

    postRequest = async(url, data, config={}) => {
        try{
            this.getHeaders(config)
            let response = await axiosInstance.post(url, data, {
                headers: this.header
            });
            return response
        }catch(error){
            console.log('Error in POST Request', error);
            throw error
        }
    }

    getRequest = async(url, config={}) => {
        try{
            this.getHeaders(config);
            let response = await axiosInstance.get(url, {
                headers: this.headers
            })
            return response
        }catch(error){
            throw error
        }
    }
}
export default HttpService;