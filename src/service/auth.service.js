import HttpService from "./axios.service";

class AuthService extends HttpService{
    login = async(data) => {
        try{
            let response = await this.postRequest('auth/login', data);
            let result = response.result
            let local_user = {
                name: result.user.name,
                email: result.user.email,
                role: result.user.role,
                user_id: result.user._id
            }
            localStorage.setItem("auth_token", result.access_token);
            return local_user
        }catch(error){
            throw error
        }
    }

    register = async(data) => {
        try{
            let result = await this.postRequest('auth/register', data)
            return result
        }catch(error){
            throw error
        }
    }
}
export const auth_svc = new AuthService();
export default AuthService;