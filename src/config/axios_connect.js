import axios from "axios";
import { toast } from "react-toastify";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_NOT_SECRET_URL,
  timeout: 15000,
  timeoutErrorMessage: "Server timed out ...",
  headers: {
    "content-type": "application/json",
  },
});

axiosInstance.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    if (error.response.status === 401) {
      localStorage.removeItem("auth_token");
      window.location.href = "/";
    } else if (error.response.status === 404 || error.response.status === 403) {
      toast.error(error.response.data.msg);
    } else {
      throw error;
    }
  }
);

export default axiosInstance;
