import { BrowserRouter, Route, Routes } from "react-router-dom";
import Layout from "./pages/layout";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
const Routing = () => {
    return(
        <>
        <ToastContainer />
            <BrowserRouter>
                <Routes>
                    <Route path= {'/'} element={<Layout.LoginPageLayout />} />
                    <Route path= {'/register'} element={<Layout.RegisterPageLayout />} />
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default Routing;